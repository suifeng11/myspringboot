package com.test.dao;

import com.test.dto.DataDongGuan;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Administrator on 2017/4/28.
 */
public interface MongoDBDao extends MongoRepository<DataDongGuan,Long> {
    DataDongGuan findById(int id);
}
