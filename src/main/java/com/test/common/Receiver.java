package com.test.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017/5/3.
 * RabbitMq测试---消费者
 */
@Component
@RabbitListener(queues = "hello")
public class Receiver {
    private static final Log log = LogFactory.getLog(Receiver.class);

    @RabbitHandler
    public void process(String hello){
        log.debug("消费者接收到消息=======" + hello);
        System.out.println("消费者接收到消息=======" + hello);
    }
}
