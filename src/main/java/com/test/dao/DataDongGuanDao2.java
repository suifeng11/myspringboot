package com.test.dao;

import com.test.dto.DataDongGuan;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Administrator on 2017/4/28.
 */
@Mapper
public interface DataDongGuanDao2 {
    @Cacheable(value = "dongguan")
    List<DataDongGuan> findAll();

    @CachePut(value = "dongguan")
    int save(DataDongGuan dataDongGuan);
}
