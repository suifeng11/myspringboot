package com.test.controller;

import com.test.common.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Administrator on 2017/5/3.
 */
@RequestMapping("/rabbitMq")
@Controller
public class RabbitController {
    @Autowired
    private Sender sender;

    @RequestMapping("/send")
    @ResponseBody
    public String send(){
        String data = "传个消息。";
        sender.send(data);
        return data;
    }
}
