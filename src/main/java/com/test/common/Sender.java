package com.test.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by Administrator on 2017/5/3.
 * rabbitMQ测试---生成者
 */
@Component
public class Sender {
    private static final Log log = LogFactory.getLog(Sender.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send(String data){
        data = data + new Date();
        log.debug("存入消息队列======" + data);
        System.out.println("存入消息队列======" + data);
        this.amqpTemplate.convertAndSend("helle",data);
    }
}
