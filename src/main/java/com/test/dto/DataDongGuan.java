package com.test.dto;

import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/4/25.
 */
public class DataDongGuan implements Serializable{
    @Id   //用于mongoDB
    private int id;
    private int houseNum;
    private int totalNum;
    private int type;
    private String date;

    public DataDongGuan() {
    }

    public DataDongGuan(int id, int houseNum, int totalNum, int type, String date) {
        this.id = id;
        this.houseNum = houseNum;
        this.totalNum = totalNum;
        this.type = type;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHouseNum() {
        return houseNum;
    }

    public void setHouseNum(int houseNum) {
        this.houseNum = houseNum;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
